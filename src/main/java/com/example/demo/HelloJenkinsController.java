package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloJenkinsController {

    @GetMapping(path = "/helloJenkins")
    public String helloJenkins(){
        return "Hello Jenkins";
    }
}
